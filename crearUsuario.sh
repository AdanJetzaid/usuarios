#!/bin/bash
ROOT_UID=0
SUCCESS=0

# Run as root, of course. (this might not be necessary, because we have to run the script somehow with root anyway)
if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  

file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe indicar el archivo con el listado de usuarios a ingresar..."
   exit 1
fi

# Del archivo con el listado de usuarios a eliminar:

# Este es el formato:
# ejemplo
#    |   
#    f1  
#$f1 = username
#$f2 = password
#$f3 = User ID 
#$f4 = Group ID
#$f5 = User ID Info
#$f6 = home directory 
#$f7 = comand shell
crearUsuario(){
	#echo "----> Ingresar Usuario <----"
	eval user="$1"
	#echo "Username 		  = ${user}"
	#echo "-------------------------"
	eval pass="$2"
	#echo "Password                   =${pass}"
	#echo "-------------------------"
	eval usid="$3"
	#echo "User ID  		  = ${usid}"
	#echo "-------------------------"
	eval group="$4"
	#echo "Group ID 		  = ${group}"
	#echo "-------------------------"
	eval inf="$5"
	#echo "User ID Info 		  = ${info}"
	#echo "-------------------------"
	eval home="$6"
	#echo "Home directory 		  = ${home}"
	#echo "-------------------------"
	eval shell="$7"
	#echo "comand shell 		  = ${shell}"

	useradd "${user}" -p "${pass}" -u "${usid}" -g "${group}" -c "${inf}" -d "${home}" -s "${shell}"
	if [ $? -eq $SUCCESS ];
	then
		echo "Usuario [${user}] ingresado correctamente..."
	else
		echo "Usuario [${user}] No se pudo ingresar..."
	fi
}

while IFS=, read -r f1 f2 f3 f4 f5 f6 f7
do
	
	crearUsuario "\${f1}" "\${f2}" "\${f3}" "\${f4}" "\${f5}" "\${f6}" "\${f7}"
        

done < ${file}

exit 0
