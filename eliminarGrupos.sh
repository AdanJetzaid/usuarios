file=$1

if [ "${file}X" = "X" ];
then
   echo "Debe indicar el archivo con el listado de Grupos a eliminar..."
   exit 1
fi

# Del archivo con el listado de grupos a eliminar:
# Este es el formato:
# ejemplo
#    |   
#    f1  
#$f1 = nombreGrupo

eliminarGrupo(){
	#echo "----> Eliminar Grupo <----"
	eval nombreGrupo="$1"
	#echo "nombreGrupo 		  = ${nombreGrupo}"
	#echo "-------------------------"

	groupdel "${nombreGrupo}"
	if [ $? -eq $SUCCESS ];
	then
		echo "Grupo [${nombreGrupo}] eliminado correctamente..."
	else
		echo "Grupo [${nombreGrupo}] No se pudo eliminar..."
	fi
}

while IFS=: read -r f1
do
	eliminarGrupo "\${f1}"	
done < ${file}

exit 0
